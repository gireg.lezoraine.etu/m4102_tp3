# Ressource Pizzas

Ressource représentant les pizzas et leurs ingrédients dans l'API.

## Méthodes de l'API

| Méthode | URI | Description |
|:--------|:----|:------------|
|GET|/pizzas|Obtenir la liste des pizzas|
|GET|/pizzas/{id}|Obtenir une pizza par son identifiant unique|
|GET|/pizzas/{id}/name|Obtenir le nom d'une pizza par son identifiant|
|GET|/pizzas/{id}/ingredients|Obtenir les ingrédients de la pizza|
|POST|/pizzas|Créer une nouvelle pizza|
|POST|/pizzas/ingredient|Ajouter un ingrédient à une pizza|
|DELETE|/pizzas/{id}|Supprimer une pizza (et ses ingrédients)|
|DELETE|/pizzas/ingredients/{id}|Supprimer un ingrédient d'une pizza|

## Représentation des données

### Pizza

Une pizza est représentée en json par un objet ayant un nom et un identifiant unique.

```json
{
  "id": 5,
  "name": "reine"
}
```

Lors de la création d'une nouvelle pizza, une donnée comme ceci doit être fournie :

```json
{ "name": "reine" }
```

### Ingrédients d'une pizza

Un ingrédient d'une pizza est stoquée dans une table à part associant un identifiant
d'une pizza et un identifiant d'un ingrédient. Chaque association est unique.

Lors de la création d'une nouvelle association, une donnée de même forme doit être fournie.

```json
{
  "pizza_id": 5,
  "ingredient_id": 10
}
```