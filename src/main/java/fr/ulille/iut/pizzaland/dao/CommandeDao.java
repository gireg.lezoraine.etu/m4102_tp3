package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.beans.Commande;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface CommandeDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (\n" +
            "id INTEGER PRIMARY KEY,\n" +
            "nom TEXT NOT NULL,\n" +
            "pizza_id INTEGER NOT NULL,\n" +
            "CONSTRAINT fk_pizza_commande_id FOREIGN KEY (pizza_id) REFERENCES pizzas_names(id))")
    void createTable();

    @SqlUpdate("DROP TABLE IF EXISTS commandes")
    void deleteTable();

    @SqlQuery("SELECT * FROM commandes")
    List<Commande> getAllCommandes();

    @SqlQuery("SELECT * FROM commandes WHERE id = :id")
    Commande getCommandeById(long id);

    @SqlQuery("SELECT * FROM commandes WHERE nom = :nom")
    List<Commande> getCommandesByNom(String nom);

    @SqlUpdate("DELETE FROM commandes WHERE id = :id")
    void deleteCommande(long id);

    @SqlUpdate("INSERT INTO commandes (nom, pizza_id) VALUES (:nom, :pizza_id)")
    @GetGeneratedKeys
    long createCommande(String nom, long pizza_id);

}
