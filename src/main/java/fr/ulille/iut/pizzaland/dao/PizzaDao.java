package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import javax.ws.rs.GET;
import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas_names (id INTEGER PRIMARY KEY, name VARCHAR)")
    void createSubTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (\n" +
                      "pizza_id INTEGER,\n" +
                      "ingredient_id INTEGER,\n" +
                      "CONSTRAINT fk_pizza_id FOREIGN KEY (pizza_id) REFERENCES pizzas_name(id),\n" +
                      "CONSTRAINT fk_ingredient_id FOREIGN KEY (ingredient_id) REFERENCES ingredients(id),\n" +
                      "CONSTRAINT pk_pid_iid PRIMARY KEY (pizza_id, ingredient_id))")
    void createPizzaTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizzas_name")
    void dropSubTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropPizzaTable();

    @SqlQuery("SELECT * FROM pizzas_names")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM pizzas_names WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza getById(long id);

    @SqlQuery("SELECT * FROM pizzas_names WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza getByName(String name);

    @SqlQuery("SELECT UNIQUE * FROM pizzas WHERE pizza_id = :pizza_id")
    @RegisterBeanMapper(Ingredient.class)
    List<Ingredient> getPizzaIngredients(long pizza_id);

    @SqlQuery("SELECT UNIQUE * FROM pizzas WHERE ingredient_id = :ingredient_id")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getPizzasWithIngredient(long ingredient_id);

    /*
     * Créer une nouvelle pizza
     */
    @SqlUpdate("INSERT INTO pizzas_names (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);

    /*
     * Insérer une association Pizza <-> Ingrédient
     */
    @SqlUpdate("INSERT INTO pizzas (pizza_id, ingredient_id) VALUES (:pid , :iid )")
    void addPizzaIngredient(long pid, long iid);

    @SqlUpdate("DELETE FROM pizzas_names WHERE id = :id")
    void removePizzaName(long id);

    @SqlUpdate("DELETE FROM pizzas WHERE pizza_id = :pid AND ingredient_id = :iid")
    void removePizzaIngredient(long pid, long iid);
}
