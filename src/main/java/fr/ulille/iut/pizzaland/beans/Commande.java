package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {

    private String nom;
    private long id;
    private long pizza_id;

    public Commande() {
    }

    public Commande(String nom, long id, long pizza_id) {
        this.nom = nom;
        this.id = id;
        this.pizza_id = pizza_id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPizza_id() {
        return pizza_id;
    }

    public void setPizza_id(long pizza_id) {
        this.pizza_id = pizza_id;
    }

    public static CommandeDto toDto(Commande c) {
        CommandeDto res = new CommandeDto();
        res.setId(c.getId());
        res.setNom(c.getNom());
        res.setPizza_id(c.getPizza_id());
        return res;
    }

    public static Commande fromDto(CommandeDto dto) {
        Commande c = new Commande();
        c.setId(dto.getId());
        c.setNom(dto.getNom());
        c.setPizza_id(dto.getPizza_id());
        return c;
    }

    public static CommandeCreateDto toCreateDto(Commande c) {
        CommandeCreateDto res = new CommandeCreateDto();
        res.setName(c.getNom());
        res.setPizza_id(c.getPizza_id());
        return res;
    }

    public static Commande fromCreateDto(CommandeCreateDto dto) {
        Commande c = new Commande();
        c.setNom(dto.getName());
        c.setPizza_id(dto.getPizza_id());
        return c;
    }

}
