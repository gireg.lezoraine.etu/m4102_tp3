package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.util.List;

public class Pizza {

    private long id;
    private String name;

    public Pizza(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Pizza() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static PizzaDto toDto(Pizza p) {
        PizzaDto res = new PizzaDto();
        res.setId(p.getId());
        res.setName(p.getName());
        return res;
    }

    public static Pizza fromDto(PizzaDto p) {
        Pizza res = new Pizza();
        res.setId(p.getId());
        res.setName(p.getName());
        return res;
    }

    public static Pizza fromCreateDto(PizzaCreateDto dto) {
        Pizza res = new Pizza();
        res.setName(dto.getName());
        return res;
    }

    public static PizzaCreateDto toCreateDto(Pizza p) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(p.getName());
        return dto;
    }

}
