package fr.ulille.iut.pizzaland.dto;

public class IngredientCreateDto {

    public String name;

    public IngredientCreateDto() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
