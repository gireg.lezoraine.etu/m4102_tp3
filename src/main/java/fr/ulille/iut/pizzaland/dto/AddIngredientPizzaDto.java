package fr.ulille.iut.pizzaland.dto;

public class AddIngredientPizzaDto {

    private long pid;
    private long iid;

    public AddIngredientPizzaDto() {
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getIid() {
        return iid;
    }

    public void setIid(long iid) {
        this.iid = iid;
    }
}
