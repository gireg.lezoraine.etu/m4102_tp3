package fr.ulille.iut.pizzaland.dto;

public class CommandeCreateDto {

    private String name;
    private long pizza_id;

    public CommandeCreateDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPizza_id() {
        return pizza_id;
    }

    public void setPizza_id(long pizza_id) {
        this.pizza_id = pizza_id;
    }
}
