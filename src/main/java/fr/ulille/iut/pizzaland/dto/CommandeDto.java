package fr.ulille.iut.pizzaland.dto;

public class CommandeDto {

    private String nom;
    private long pizza_id;
    private long id;


    public CommandeDto() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getPizza_id() {
        return pizza_id;
    }

    public void setPizza_id(long pizza_id) {
        this.pizza_id = pizza_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CommandeDto(String nom, long pizza_id, long id) {
        this.nom = nom;
        this.pizza_id = pizza_id;
        this.id = id;
    }
}
