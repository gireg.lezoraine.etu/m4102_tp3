package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.AddIngredientPizzaDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/pizzas")
public class PizzaResource {

    private PizzaDao dao;
    private IngredientDAO idao;

    @Context
    private UriInfo uriInfo;

    public PizzaResource() {
        dao = BDDFactory.buildDao(PizzaDao.class);
        idao = BDDFactory.buildDao(IngredientDAO.class);
        idao.createTable();
        dao.createSubTable();
        dao.createPizzaTable();
    }

    @GET
    public List<PizzaDto> getAll() {
        try {
            List<Pizza> pizzas = dao.getAll();
            return pizzas.stream().map(Pizza::toDto).collect(Collectors.toList());
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/{id}")
    public PizzaDto getById(@PathParam("id") long id) {
        try {
            Pizza p = dao.getById(id);
            return Pizza.toDto(p);
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("/{id}/name")
    public String getPizzaName(@PathParam("id") long id) {
        try {
            Pizza p = dao.getById(id);
            return p.getName();
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("/{id}/ingredients")
    public List<IngredientDto> getPizzaIngredients(@PathParam("id") long id) {
        try {
            return dao.getPizzaIngredients(id).stream().map(Ingredient::toDto).collect(Collectors.toList());
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    /*
        Méthodes POST
     */

    @POST
    public Response createNewPizzaName(PizzaCreateDto dto) {
        try {
            Pizza existing = dao.getByName(dto.getName());
            if (existing != null) {
                throw new WebApplicationException(Response.Status.CONFLICT);
            }

            Pizza p = Pizza.fromCreateDto(dto);
            long id = dao.insert(dto.getName());
            p.setId(id);
            PizzaDto pdto = Pizza.toDto(p);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();
            return Response.created(uri).entity(pdto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

    @POST
    @Path("/ingredients")
    public Response addIngredientToPizza(AddIngredientPizzaDto dto) {
        try {
            Pizza existingPizza = dao.getById(dto.getPid());
            Ingredient existingIngredient = idao.findById(dto.getIid());

            if (existingIngredient == null && existingPizza == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            dao.addPizzaIngredient(dto.getPid(), dto.getIid());
            return Response.status(Response.Status.CREATED).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

    /*
        Méthodes DELETE
     */

    @DELETE
    @Path("/{id}")
    public Response deletePizzaName(@PathParam("id") long id) {
        if (dao.getById(id) == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        dao.removePizzaName(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }

    @DELETE
    @Path("/{pid}/ingredients/{iid}")
    public Response deletePizzaIngredient(@PathParam("pid") long pid, @PathParam("iid") long iid) {
        dao.removePizzaIngredient(pid, iid);
        return Response.status(Response.Status.ACCEPTED).build();
    }
}
