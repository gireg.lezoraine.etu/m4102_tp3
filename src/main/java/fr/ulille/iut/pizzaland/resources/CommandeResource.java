package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/commandes")
public class CommandeResource {

    private CommandeDao cdao;

    @Context
    private UriInfo uriInfo;

    public CommandeResource() {
        cdao = BDDFactory.buildDao(CommandeDao.class);
        cdao.createTable();
    }

    @GET
    @Path("/")
    public List<CommandeDto> getAll() {
        try {
            List<Commande> commandes = cdao.getAllCommandes();
            return commandes.stream().map(Commande::toDto).collect(Collectors.toList());
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/{id}")
    public CommandeDto getById(@PathParam("id") long id) {
        try {
            Commande c = cdao.getCommandeById(id);
            return Commande.toDto(c);
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("/{id}/client")
    public String getClient(@PathParam("id") long id) {
        try {
            Commande c = cdao.getCommandeById(id);
            return c.getNom();
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("/{id}/pizza")
    public long getPizza(@PathParam("id") long id) {
        try {
            Commande c = cdao.getCommandeById(id);
            return c.getPizza_id();
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("/client/{client}")
    public List<CommandeDto> getCommandesFromClient(@PathParam("client") String client) {
        try {
            List<Commande> commandes = cdao.getCommandesByNom(client);
            return commandes.stream().map(Commande::toDto).collect(Collectors.toList());
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @POST
    public Response createNewCommande(CommandeCreateDto dto) {
        try {

            Commande c = Commande.fromCreateDto(dto);
            long id = cdao.createCommande(dto.getName(), dto.getPizza_id());
            c.setId(id);
            CommandeDto returnDto = Commande.toDto(c);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();
            return Response.created(uri).entity(returnDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCommande(@PathParam("id") long id) {
        try {
            cdao.deleteCommande(id);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

}
