package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dto.IngredientDto;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class IngredientResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
    
    private IngredientDAO dao;
    
    @Override
    protected Application configure() {
    	BDDFactory.setJdbiForTests();
    	return new ApiV1();
    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la base de données
    // et les DAO
    
    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(IngredientDAO.class);
    	dao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }

    @Test
    public void testGetEmptyList() {
    	LOGGER.log(Level.ALL, "testgetEmptyList");
	// La méthode target() permet de préparer une requête sur une URI.
	// La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/ingredients").request().get();

	// On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		  
	// On vérifie la valeur retournée (liste vide)
	// L'entité (readEntity() correspond au corps de la réponse HTTP.
	// La classe javax.ws.rs.core.GenericType<T> permet de définir le type
	// de la réponse lue quand on a un type complexe (typiquement une liste).
	List<IngredientDto> ingredients;
        ingredients = response.readEntity(new GenericType<List<IngredientDto>>(){});

        assertEquals(0, ingredients.size());

    }
    
    @Test
    public void getExistingIngredient() {
		LOGGER.log(Level.ALL, "getExistingIngredient");
    	Ingredient i = new Ingredient();
    	i.setName("Mozzarella");
    	
    	long id = dao.insert(i.getName());
    	i.setId(id);
    	
    	Response reponse = target("/ingredients/" + id).request().get();
    	assertEquals(Response.Status.OK.getStatusCode(), reponse.getStatus());
    	
    	Ingredient result = Ingredient.fromDto(reponse.readEntity(IngredientDto.class));
    	assertEquals(i, result);
    }

    /*
    	Methodes POST
     */

    @Test
    public void testCreateIngredient() {
		LOGGER.log(Level.ALL, "testCreateIngredient");
    	IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
    	ingredientCreateDto.setName("Chorizo");
    	
    	Response response = target("/ingredients")
    			.request()
    			.post(Entity.json(ingredientCreateDto));
    	
    	assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    	
    	IngredientDto returned = response.readEntity(IngredientDto.class);
    	
    	assertEquals(target("/ingredients/" + returned.getId()).getUri(), response.getLocation());
    	
    	assertEquals(returned.getName(), ingredientCreateDto.getName());
    }
    
    @Test
    public void testCreateSameIngredient() {
		LOGGER.log(Level.ALL, "testCreateSameIngredient");
    	IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
    	ingredientCreateDto.setName("Chorizo");
    	dao.insert(ingredientCreateDto.getName());
    	
    	Response response = target("/ingredients")
    			.request()
    			.post(Entity.json(ingredientCreateDto));
    	
    	assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testCreateIngredientWithoutName() {
		LOGGER.log(Level.ALL, "testCreateIngredientWithoutName");
    	IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
    	Response response = target("/ingredients")
    			.request()
    			.post(Entity.json(ingredientCreateDto));
    	
    	assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

    /*
    Méthode DELETE
     */

    @Test
	public void testDeleteExistingIngredient() {
		LOGGER.log(Level.ALL, "testDeleteExistingIngredient");
    	// Creation de la ressource dans la base de donnée
    	Ingredient ingredient = new Ingredient();
    	ingredient.setName("mozarella");
    	long id = dao.insert(ingredient.getName());
    	ingredient.setId(id);

    	// Test
		Response response = target("/ingredients/" + id).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Ingredient result = dao.findById(id);
		assertEquals(null, result);
	}

	@Test
	public void testDeleteNotExistingIngredient() {
		LOGGER.log(Level.ALL, "testDeleteNotExistingIngredient");
    	// Aucune ressource n'est crée

		Response response = target("/ingredients/125").request().delete();

		// Erreur 404 reçue
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetIngredientName() {
		LOGGER.log(Level.ALL, "testGetIngredientName");
    	// Création de la ressource
		Ingredient ingredient = new Ingredient();
		ingredient.setName("mozzarella");
		long id = dao.insert(ingredient.getName());

		Response response = target("/ingredients/" + id + "/name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals("mozzarella", response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingIngredientName() {
		LOGGER.log(Level.ALL, "testGetNotExistingIngredientName");
    	Response response = target("/ingredients/125/name").request().get();

    	assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
}
