package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

public class PizzaResourceTest extends JerseyTest {

    private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());


    private PizzaDao dao;
    private IngredientDAO idao;

    /*
        Setup de l'environnement de test
     */

    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        return new ApiV1();
    }

    @Before
    public void setEnvUp() {
        dao = BDDFactory.buildDao(PizzaDao.class);
        idao = BDDFactory.buildDao(IngredientDAO.class);
        LOGGER.log(Level.WARNING, "DAO [" + dao + "]");
        LOGGER.log(Level.WARNING, "IDAO [" + dao + "]");
        idao.createTable();
        dao.createSubTable();
        dao.createPizzaTable();
    }

    @After
    public void tearEnvDown() throws Exception {
        LOGGER.log(Level.WARNING, "DAO [" + dao + "]");
        LOGGER.log(Level.WARNING, "IDAO [" + dao + "]");
        dao.dropPizzaTable();
        dao.dropSubTable();
        idao.dropTable();
    }

    /*
        Tests de la méthode GET
     */

    @Test
    public void testGetEmptyPizzaList() {
        LOGGER.log(Level.ALL, "testGetEmptyPizzaList");
        Response response = target("/pizzas").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>(){});
        assertEquals(0, pizzas.size());
    }

    @Test
    public void getExistingPizza() {
        LOGGER.log(Level.ALL, "getExistingPizza");
        Pizza p = new Pizza();
        p.setName("Avalanche");
        long id = dao.insert(p.getName());
        p.setId(id);

        Response response = target("/pizzas/" + id).request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(p, result);
    }

    @Test
    public void getNonExistingPizza() {
        LOGGER.log(Level.ALL, "getNonExistingPizza");
        Response response = target("/pizzas/1561").request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
}
