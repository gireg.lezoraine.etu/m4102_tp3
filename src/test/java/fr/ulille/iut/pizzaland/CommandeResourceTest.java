package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.resources.CommandeResource;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

public class CommandeResourceTest extends JerseyTest {

    private static final Logger LOGGER = Logger.getLogger(CommandeResourceTest.class.getName());


    private CommandeDao dao;
    private IngredientDAO idao;
    private PizzaDao pdao;

    /*
        Setup de l'environnement de test
     */

    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        return new ApiV1();
    }

    @Before
    public void setEnvUp() {
        pdao = BDDFactory.buildDao(PizzaDao.class);
        dao = BDDFactory.buildDao(CommandeDao.class);
        idao = BDDFactory.buildDao(IngredientDAO.class);
        idao.createTable();
        pdao.createSubTable();
        pdao.createPizzaTable();
        dao.createTable();

        pdao.insert("Reine");
    }

    @After
    public void tearEnvDown() throws Exception {
        dao.deleteTable();
        pdao.dropPizzaTable();
        pdao.dropSubTable();
        idao.dropTable();
    }

    /*
        Tests de la méthode GET
     */

    @Test
    public void testGetEmptyCommandeList() {
        LOGGER.log(Level.ALL, "getEmptyCommandList");
        Response response = target("/commandes").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        List<CommandeDto> commandes;
        commandes = response.readEntity(new GenericType<List<CommandeDto>>(){});
        assertEquals(0, commandes.size());
    }

    @Test
    public void testGetCommandeList() {
        LOGGER.log(Level.ALL, "getCommandList");
        Commande c1 = new Commande();
        Commande c2 = new Commande();
        c1.setNom("John");
        c2.setNom("Jerry");
        c1.setPizza_id(1);
        c2.setPizza_id(2);
        c1.setId(dao.createCommande(c1.getNom(), c1.getPizza_id()));
        c2.setId(dao.createCommande(c2.getNom(), c2.getPizza_id()));

        Response response = target("/commandes").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        List<CommandeDto> commandes = response.readEntity(new GenericType<List<CommandeDto>>(){});
        assertEquals(2, commandes.size());
    }

    @Test
    public void getExistingCommande() {
        LOGGER.log(Level.ALL, "getExistingCommand");
        Commande c = new Commande();
        c.setNom("John");
        c.setPizza_id(1);
        long id = dao.createCommande(c.getNom(), c.getPizza_id());
        c.setId(id);

        Response response = target("/commandes/" + id).request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
        assertEquals(c, result);
    }

    @Test
    public void getNonExistingCommande() {
        LOGGER.log(Level.ALL, "getNonExistingCommand");
        Response response = target("/commandes/1561").request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void getPizzaNomClient() {
        LOGGER.log(Level.ALL, "getPizzaNomClient");
        Commande c = new Commande();
        c.setNom("John");
        c.setPizza_id(1);
        c.setId(dao.createCommande(c.getNom(), c.getPizza_id()));

        Response res = target("/commandes/1/client").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());
        assertEquals("John", res.readEntity(String.class));
    }

    @Test
    public void getPizzaIdFromCommande() {
        LOGGER.log(Level.ALL, "getPizzaIdFromCommande");
        Commande c = new Commande();
        c.setNom("John");
        c.setPizza_id(1);
        c.setId(dao.createCommande(c.getNom(), c.getPizza_id()));

        Response res = target("/commandes/1/pizza").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());
        assertEquals(new Long(1), res.readEntity(Long.class));
    }

    @Test
    public void testCreateNewCommande() {
        LOGGER.log(Level.ALL, "createNewCommande");
        CommandeCreateDto create = new CommandeCreateDto();
        create.setName("John");
        create.setPizza_id(1);
        Response res = target("/commandes")
                .request()
                .post(Entity.json(create));

        assertEquals(Response.Status.CREATED.getStatusCode(), res.getStatus());

        Response res2 = target("/commandes/1").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), res2.getStatus());

        CommandeDto commande = res2.readEntity(CommandeDto.class);
        assertEquals(1, commande.getId());
    }

    @Test
    public void testDeleteInexistingCommande() {
        LOGGER.log(Level.ALL, "testDeleteInexistingCommande");
        Response res = target("/commandes/1564").request().delete();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), res.getStatus());
    }

    @Test
    public void testDeleteCommande() {
        LOGGER.log(Level.ALL, "testDeleteCommande");
        Commande c = new Commande();
        c.setNom("John");
        c.setPizza_id(1);
        c.setId(dao.createCommande(c.getNom(), c.getPizza_id()));

        Response res = target("/commandes").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

        List<CommandeDto> commandes = res.readEntity(new GenericType<List<CommandeDto>>(){});
        assertEquals(1, commandes.size());

        Response res2 = target("/commandes/1").request().delete();
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), res2.getStatus());

        commandes = res2.readEntity(new GenericType<List<CommandeDto>>(){});
        assertEquals(0, commandes.size());
    }
}
