# Ressource Commandes

Ressource des commandes de l'API

## Méthodes de l'API

| Méthode | URI | Description |
|:--------|:----|:------------|
|GET|/commandes|Obtenir la liste des commandes|
|GET|/commandes/{id}|Obtenir les informations d'une commande par son id|
|GET|/commandes/{id}/client|Obtenir le nom du client de la commande|
|GET|/commandes/{id}/pizza|Obtenir l'id de la pizza de la commande|
|GET|/commandes/client/{client}|Obtenir toutes les commandes d'un client|
|POST|/commandes|Créer une nouvelle commande|
|DELETE|/commandes/{id}|Supprime une commande de l'API|


## Représentation des données

### Commande

Une commande est représentée en json par un objet ayant un nom de client, un identifiant unique et un identifiant de la pizza commandée.

```json
{
  "id": 5,
  "name": "John",
  "pizza_id": 5
}
```

Lors de la création d'une nouvelle commande, une donnée de cette forme doit être fournie:

```json
{ 
  "name": "John",
  "pizza_id": 5
}
```